const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 4000;
const rts = require(`./routes/router`)


//body parser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true,
})
)

app.use(`/api/:vendor`, (request, response, next) => {
  //console.log(request.params.vendor,'params')
  next()
})
app.param(`vendor`, (request, response, next, val) => {
  //console.log(`from param method`,val)
  response.locals.vendorname = val
  //console.log(response.locals.vendorname,` vanduruchu `)
  next()
})


app.use(`/api/:vendor`, rts)
app.get('/', (request, response) => {
  response.json('postgres Multi tenency').status(200);
})

//Port details
app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})