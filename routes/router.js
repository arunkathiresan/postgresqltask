const express = require('express')
const router = express.Router()
const mydb = require(`../controller/queries`)

// router.use(`/:vendor`,(Request,Response,next)=>{
//     console.log(Request.params)
//     next()
// })
// router.param(`vendor`,(request,response,next,val)=>{
//     console.log(`from param method`,val)
//     response.locals.vendorname = val
//     console.log(response.locals.vendorname,` vanduruchu `)
//     next()
// })
router.post('/', mydb.createVendor)
router.post('/createorder', mydb.createOrder)
router.post('/addorder', mydb.placeOrder)
router.get('/', mydb.getOrderDetails);
router.delete('/', mydb.deleteOrder);

module.exports = router