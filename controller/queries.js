const Pool = require('pg').Pool

const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'example',
    password: 'arun',
    port: 5432,
})


createVendor = (request, response) => {
    const vendorName = response.locals.vendorname;
    pool.query(`CREATE SCHEMA IF NOT EXISTS ${vendorName}`, (error, results) => {
        if (error) {
            response.status(500).json({
                message: "Schema Not created",
                error: error
                })
                throw error
        } else {
            response.status(201).json(`schema for ${vendorName} created sucessfully`)
            console.log(`schema created sucessfully`)
        }
    })
}

//creating a table structure for each schema
createOrder = (request, response) => {
    const vendorName = response.locals.vendorname;
    pool.query(`CREATE TABLE ${vendorName}.orderDetails(orderId serial PRIMARY KEY, foodName varchar(30),place varchar(20), mobileNo numeric,amount numeric);`, (error, results) => {
        if (error) {
            response.status(500).json({
                message: "Table not created",
                error: error
            })
        } else {
            response.status(200).json(`Table for ${vendorName}.Orderdetails created sucessfully`)
            console.log(`Table created sucessfully`)
        }
    })
}

//Add order details for placing an order
placeOrder = (request, response) => {
    const vendorName = response.locals.vendorname;
    const foodName = request.body.name;
    const place = request.body.place;
    const mobileNo = request.body.mobno;
    const amount = request.body.amount;
    pool.query(`INSERT INTO ${vendorName}.Orderdetails(foodName,place,mobileNo,amount) VALUES ('${foodName}','${place}','${mobileNo}','${amount}'); `, (error, results) => {
        if (error) {
            response.status(500).json({
                message: "Data not inserted",
                error: error
            })
        } else {
            response.status(202).json(`sucessfully Order Placed`)
        }
    })
}

//get all order details of each schema separatly
getOrderDetails = (request, response) => {
    const vendorName = response.locals.vendorname;
    //console.log(response.locals.vendorname)
    pool.query(`SELECT * FROM ${vendorName}.Orderdetails`, (error, results) => {
        if (error) {
            response.status(204).json({
                message: "Nothing to display",
                error: error
            })
        } else {
            response.status(200).json(results.rows)
        }
    })
}

//delete the placed order using order id
deleteOrder = (request, response) => {
    const vendorName = response.locals.vendorname;
    const orderId = request.body.id;
    pool.query(`DELETE FROM ${vendorName}.Orderdetails WHERE orderId = ${orderId}`, (error, results) => {
        if (error) {
            response.status(500).json({
                message: "Data not deleted",
                error: error
            })
        } else {
            response.status(200).json("deleted succussfully");
            console.log("deleted succesfully");
        }
    })
}

module.exports = {
    createVendor,
    createOrder,
    placeOrder,
    getOrderDetails,
    deleteOrder,
}